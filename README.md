# Table of contents

- [Table of contents](#table-of-contents)
- [Getting started](#getting-started)
  - [<a name="installation"></a> Installation](#-installation)
      - [Node](#node)
      - [Gulp (CLI)](#gulp-cli)
      - [Dependencies](#dependencies)
  - [<a name="local-development"></a> Local development](#-local-development)
  - [<a name="sandbox-production"></a> Sandbox + Production](#-sandbox--production)

# Getting started

## <a name="installation"></a> Installation

#### Node

_Current version: v13.6.0_

https://changelog.com/posts/install-node-js-with-homebrew-on-os-x

#### Gulp (CLI)

_Current version: v2.2.0_

```
$ npm install -g gulp-cli
```

#### Dependencies

```
npm install
```

## <a name="local-development"></a> Local development

```
$ gulp --project folder-name
```

- Builds the `src` project files to a `dev` folder, launches a local [Browsersync](https://browsersync.io) server in a browser, watches for file changes, and reloads the page when changes are detected.

## <a name="sandbox-production"></a> Sandbox + Production

```
$ gulp build --project folder-name
```

- Initiates the build stream with a prompt, asking which environment to build for.

```
[1] Sandbox
[2] Production

Which environment would you like to build for?: _
```

- Once an environment has been selected, the stream builds the `src` project files to a `dev` folder and a `dist` folder, without launching a local server or watching for file changes.
- Selecting the `sandbox` environment adds an additional prompt to the end of the stream, asking for a sandbox name. When a sandbox has been entered, the stream will `rsync` the `dist/assets` folder to the sandbox.

```
Which sandbox would you like to sync to?: _
```

- Check with the [Studio Development](https://chat.google.com/room/AAAALjGwZVc) team, regarding [sandbox availability](https://docs.google.com/spreadsheets/d/1aF9Qaoxo6rk4H_FcEKTx0VQAOVPLUXBO64fh18NVrGo/edit#gid=0).