/* ============================== */
/*  WORKING LIST
/* ============================== */

// TODO: Add CSS formatting (CSScomb / Beautify / Prettier)
// TODO: Clean up TODOs and FIXMEs


/* ============================== */
/*  CONSTANTS
/* ============================== */

// Dependencies
const { src, dest, lastRun, watch, series } = require('gulp');
const $ = require('gulp-load-plugins') ({
    pattern: ['*'],
    scope: ['devDependencies']
});
const browserSync = require('browser-sync').create();
const path = require('path');
const pkg = require('./package.json');
const project = process.argv.indexOf('--project') == -1 ? '**/' : process.argv[process.argv.indexOf('--project') + 1] + '/';

/* ============================== */
/*  BANNER
/* ============================== */

const banner = [
    '/* ========================================================================================== */\n' +
    '/*  Project: <%= package.title %>\n' +
    '/*  Author: <%= package.author.name %>\n' +
    '/*  Date: ' + new Date() + '\n' +
    '/*  \n' +
    '/*  Copyright © ' + new Date().getFullYear() + ' <%= package.author.company %>\n' +
    '/*  <%= package.author.url %>\n' +
    '/* ========================================================================================== */\n\n'
].join('\n');


/* ============================== */
/*  INITIALIZE
/* ============================== */

/* ========================================================================================== */
/*  This task checks that a project has been defined using the "--project" parameter, and
/*  then deletes any pre-existing "dev" and "dist" folders for a fresh build. If a project
/*  hasn't been defined, the stream is stopped and an error is thrown.
/* ========================================================================================== */

function init() {
    if (process.argv.indexOf('--project') == -1 || !process.argv[process.argv.indexOf('--project') + 1]) {
        throw new Error('You must define a project using the "--project" parameter. Refer to the README for help.');
    }

    return $.del([
        pkg.paths.dev.root,
        pkg.paths.dist.root
    ]);
}




/* ============================== */
/*  COPY
/* ============================== */

/* ========================================================================================== */
/*  This task copies the "src" folder's root "index.html" file to the destination path. The
/*  file is then injected with project-specific HTML files and launched by Browsersync, via
/*  other tasks in the series.
/* ========================================================================================== */

function copy() {
    return src([
        pkg.paths.src.root + 'index.html'
    ])
        .pipe(dest([
            pkg.paths.repo.root
        ]));
}


/* ============================== */
/*  HTML
/* ============================== */

/* ========================================================================================== */
/*  This task compiles source HTML files, places the files in the destination path, and
/*  reloads the page.
/* ========================================================================================== */

// FIXME: Compiling all files, instead of changed files

function html() {
    return src([
        pkg.paths.src.projects + project + '**/*.html',
        '!' + pkg.paths.src.projects + project + '**/partials/**/*.html',
        '!' + pkg.paths.src.projects + project + '**/shared/*.html'
    ])
        .pipe($.fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe($.cacheBust({
            type: 'timestamp'
        }))
        .pipe(dest([
            pkg.paths.dev.projects + project
        ]))
        .pipe($.ifEnv.includes('sandbox', 'production7', 'production8', dest([
            pkg.paths.dist.projects + project
        ])))
        .pipe(browserSync.stream());
}


/* ============================== */
/*  CSS
/* ============================== */

/* ========================================================================================== */
/*  This task compiles source vendor SCSS files into "vendor.css", strips comments, adds
/*  vendor prefixes, combines media queries, writes sourcemaps, places the file in the
/*  destination path, and reloads the page. If an error occurs, the "plumber" pipe prevents
/*  the stream from breaking and displays the error via popup.
/* ========================================================================================== */

// FIXME: Compiling all files, instead of changed files

function vendorCSS() {
    return src([
        pkg.paths.src.core.scss + 'vendors/**/*.scss'
    ])
        .pipe($.ifEnv.includes('development', $.plumber({
            errorHandler: $.notify.onError('Error: <%= error.message %>')
        })))
        .pipe($.sourcemaps.init())
        .pipe($.sass())
        .pipe($.stripCssComments({
            preserve: false
        }))
        .pipe($.autoprefixer({
            cascade: false
        }))
        .pipe($.groupCssMediaQueries())
        .pipe($.sourcemaps.write('./'))
        .pipe($.flatten({
            includeParents: 0
        }))
        .pipe(dest([
            pkg.paths.dev.css + 'vendors/'
        ]))
        .pipe(browserSync.stream());
}


/* ========================================================================================== */
/*  This task compiles the source core SCSS file into "styles.css", strips comments, adds
/*  vendor prefixes, combines media queries, writes sourcemaps, places the file in the
/*  destination path, and reloads the page. If an error occurs, the "plumber" pipe prevents
/*  the stream from breaking and displays the error via popup.
/* ========================================================================================== */

function coreCSS() {
    return src([
        pkg.paths.src.core.scss + '*.scss'
    ])
        .pipe($.ifEnv.includes('development', $.plumber({
            errorHandler: $.notify.onError('Error: <%= error.message %>')
        })))
        .pipe($.sourcemaps.init())
        .pipe($.sass())
        .pipe($.stripCssComments({
            preserve: false
        }))
        .pipe($.autoprefixer({
            cascade: false
        }))
        .pipe($.groupCssMediaQueries())
        .pipe($.sourcemaps.write('./'))
        .pipe($.flatten({
            includeParents: 1
        }))
        .pipe(dest([
            pkg.paths.dev.css
        ]))
        .pipe(browserSync.stream());
}


/* ========================================================================================== */
/*  This task compiles source project SCSS files into "project.css", strips comments, adds
/*  vendor prefixes, combines media queries, writes sourcemaps, places the file in the
/*  destination path, and reloads the page. If an error occurs, the "plumber" pipe prevents
/*  the stream from breaking and displays the error via popup.
/* ========================================================================================== */

// FIXME: Compiling all files, instead of changed files

function devCSS() {
    return src([
        pkg.paths.src.projects + project + 'assets/scss/**/*.scss'
    ])
        .pipe($.ifEnv.includes('development', $.plumber({
            errorHandler: $.notify.onError('Error: <%= error.message %>')
        })))
        .pipe($.sourcemaps.init())
        .pipe($.sass())
        .pipe($.stripCssComments({
            preserve: false
        }))
        .pipe($.autoprefixer({
            cascade: false
        }))
        .pipe($.groupCssMediaQueries())
        .pipe($.sourcemaps.write('./'))
        .pipe($.flatten({
            includeParents: 0
        }))
        .pipe(dest([
            pkg.paths.dev.css + 'projects/'
        ]))
        .pipe(browserSync.stream());
}


/* ========================================================================================== */
/*  This task minifies development CSS files, prepends ".min" to the filename before the
/*  extension, and places the files in the destination path.
/* ========================================================================================== */

function distCSS() {
    return src([
        pkg.paths.dev.css + '**/*.css',
        '!' + pkg.paths.dev.css + 'projects/example.css'
    ])
        .pipe($.cleanCss({
            level: {
                2: {
                    restructureRules: true
                }
            }
        }))
        .pipe($.rename({
            suffix: '.min'
        }))
        .pipe($.header(banner, {
            package: pkg
        }))
        .pipe(dest([
            pkg.paths.dist.css
        ]));
}


/* ============================== */
/*  IMAGES
/* ============================== */

/* ========================================================================================== */
/*  This task copies source image files to the destination path. If the "assets" or "img"
/*  folders don't exist within a project, the task will create them to avoid errors and
/*  breaking the stream.
/* ========================================================================================== */

// FIXME: Compiling all files, instead of changed files

function devImages() {
    if (!$.fs.existsSync(pkg.paths.src.projects + project + 'assets/')) {
        $.fs.mkdirSync(pkg.paths.src.projects + project + 'assets/');
    }

    if (!$.fs.existsSync(pkg.paths.src.projects + project + 'assets/img/')) {
        $.fs.mkdirSync(pkg.paths.src.projects + project + 'assets/img/');
    }

    return src([
        pkg.paths.src.projects + project + 'assets/img/**/*'
    ], {
        since: lastRun(devImages)
    })
        .pipe($.directorySync(pkg.paths.src.projects + project + 'assets/img/', pkg.paths.dev.img))
        .pipe(browserSync.stream());
}



/* ============================== */
/*  JS
/* ============================== */

/* ========================================================================================== */
/*  This task places source vendor JS files in the destination path, and reloads the page.
/*  If an error occurs, the "plumber" pipe prevents the stream from breaking and displays
/*  the error via popup.
/* ========================================================================================== */

// FIXME: Compiling all files, instead of changed files

function vendorJS() {
    return src([
        pkg.paths.src.core.js + 'vendors/**/*.js'
    ])
        .pipe($.ifEnv.includes('development', $.plumber({
            errorHandler: $.notify.onError('Error: <%= error.message %>')
        })))
        .pipe($.flatten({
            includeParents: 0
        }))
        .pipe(dest([
            pkg.paths.dev.js + 'vendors/'
        ]))
        .pipe(browserSync.stream());
}


/* ========================================================================================== */
/*  This task combines source core JS files into "scripts.js", places the file in the
/*  destination path, and reloads the page. If an error occurs, the "plumber" pipe prevents
/*  the stream from breaking and displays the error via popup.
/* ========================================================================================== */

function coreJS() {
    return src([
        pkg.paths.src.core.js + 'libraries/bootstrap/bootstrap.bundle.js',
        pkg.paths.src.core.js + 'scripts.js'
    ])
        .pipe($.ifEnv.includes('development', $.plumber({
            errorHandler: $.notify.onError('Error: <%= error.message %>')
        })))
        .pipe($.concat('scripts.js'))
        .pipe(dest([
            pkg.paths.dev.js
        ]))
        .pipe(browserSync.stream());
}


/* ========================================================================================== */
/*  This task places source project JS files in the destination path, and reloads the page.
/*  If an error occurs, the "plumber" pipe prevents the stream from breaking and displays
/*  the error via popup.
/* ========================================================================================== */

// FIXME: Compiling all files, instead of changed files

function devJS() {
    return src([
        pkg.paths.src.projects + project + 'assets/js/**/*.js'
    ])
        .pipe($.ifEnv.includes('development', $.plumber({
            errorHandler: $.notify.onError('Error: <%= error.message %>')
        })))
        .pipe($.flatten({
            includeParents: 0
        }))
        .pipe(dest([
            pkg.paths.dev.js + 'projects/'
        ]))
        .pipe(browserSync.stream());
}


/* ========================================================================================== */
/*  This task strips console, alert, and debugger statements from development JS files,
/*  minifies, prepends ".min" to the filename before the extension, and places the files in
/*  the destination path.
/* ========================================================================================== */

function distJS() {
    return src([
        pkg.paths.dev.js + '**/*.js',
        '!' + pkg.paths.dev.js + 'projects/example.js'
    ])
        .pipe($.ifEnv.includes('production7', 'production8', 'deploy', $.stripDebug()))
        .pipe($.uglifyEs.default())
        .pipe($.rename({
            suffix: '.min'
        }))
        .pipe($.header(banner, {
            package: pkg
        }))
        .pipe(dest([
            pkg.paths.dist.js
        ]));
}


/* ============================== */
/*  INJECT
/* ============================== */

/* ========================================================================================== */
/*  This task adds links to the development folder's root "index.html" for every compiled
/*  HTML file associated with a project.
/* ========================================================================================== */

function inject() {
    return src([
        pkg.paths.repo.root + 'index.html'
    ])
        .pipe($.inject(
            src([
                pkg.paths.dev.projects + project + '**/*.html',
                '!' + pkg.paths.dev.projects + project + '**/partials/**/*.html',
                '!' + pkg.paths.dev.projects + project + '**/shared/*.html'
            ], {
                read: false
            }), {
                empty: true,
                quiet: true,
                relative: true,
                removeTags: true,
                transform: (filepath, file) => {
                    if (filepath.slice(-5) === '.html') {
                        return '<li>\n                <a href="' + filepath + '"><span>' + project + '</span>' + file.relative + '</a>\n            </li>\n\n            ';
                    }
                }
            }
        ))
        .pipe(dest([
            pkg.paths.repo.root
        ]))
        .pipe(browserSync.stream());
}


/* ============================== */
/*  SERVE
/* ============================== */

/* ========================================================================================== */
/*  This task launches a local Browsersync server in a browser, watches for file changes,
/*  and runs the respective task when changes are detected.
/* ========================================================================================== */

function serve() {

    // Browsersync
    browserSync.init({
        notify: false,
        server: pkg.paths.repo.root
    });

    // HTML
    watch([
        pkg.paths.src.projects +  project + '**/*.html'
    ])
        .on('add', series(html, inject))
        .on('change', series(html))
        .on('unlink', (filepath) => {
            const srcPath = path.relative(path.resolve(pkg.paths.src.projects + project), filepath);
            const destPath = path.resolve(pkg.paths.dev.projects + project, srcPath);

            $.del.sync(destPath);
        })
        .on('unlink', series(inject));

    // Vendor CSS
    watch([
        pkg.paths.src.core.scss + 'vendors/**/*.scss'
    ], series(vendorCSS));

    // Core CSS
    watch([
        pkg.paths.src.core.scss + '**/*.scss'
    ], series(coreCSS));

    // Project CSS
    watch([
        pkg.paths.src.projects + project + 'assets/scss/**/*.scss'
    ], series(devCSS));

    // Images
    watch([
        pkg.paths.src.projects + project + 'assets/img/**/*'
    ], series(devImages));

    // Vendor JS
    watch([
        pkg.paths.src.core.js + 'vendors/**/*.js'
    ], series(vendorJS));

    // Core JS
    watch([
        pkg.paths.src.core.js + 'scripts.js'
    ], series(coreJS));

    // Project JS
    watch([
        pkg.paths.src.projects + project + 'assets/js/**/*.js'
    ], series(devJS));
}

/* ============================== */
/*  DEFAULT
/* ============================== */

/* ========================================================================================== */
/*  This task sequentially runs a series of tasks for local development, launches a local
/*  server, watches for file changes, and reloads the page when changes are detected.
/* ========================================================================================== */

exports.default = series(
    init,
    copy,
    html,
    vendorCSS,
    vendorJS,
    coreCSS,
    coreJS,
    devCSS,
    devImages,
    devJS,
    inject,
    serve
);

