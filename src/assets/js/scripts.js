/* ============================== */
/*  THROTTLE
*
*  Util function, modified from underscore.js
*  Use this on scroll and resize functions to prevent performance issues
*
*  $(window).on("scroll", throttle(function, waitTime));
*
/* ============================== */

function throttle(func, wait) {
    var context, args, result;
    var timeout = null;
    var previous = 0;

    var later = function(){
        previous = Date.now();
        timeout = null;
        result = func.apply(context, args);

        if (!timeout) {
            context = args = null;
        }
    };

    return function() {
        var now = Date.now();
        var remaining = wait - (now - previous);
        context = this;
        args = arguments;
        if (remaining <= 0 || remaining > wait) {
            if (timeout) {
                clearTimeout(timeout);
                timeout = null;
            }
            previous = now;
            result = func.apply(context, args);
            if (!timeout) context = args = null;
        } else if (!timeout) {
        timeout = setTimeout(later, remaining);
        }
        return result;
    };
}

(function($){

    /* ============================================================ */
    /*  FUNCTIONS
    /* ============================================================ */



    /* ========================================================================================== */
    /*  SCROLL TO REVEAL
    *
    *  REQUIRES the use of Waypoints.js
    *
    *  When the top of the .reveal target hits the bottom of the window it will be
    *  added to the reveal queue to animate in.
    * /* ========================================================================================== */

    var revealQueue = [],
        revealWait = 125, // ms between reveals
        revealTimer,
        revealPending = false;

    // TODO: Allow for order to be set?
    function reveal(target) {
        clearTimeout(revealTimer);

        // if the current target is already revealed, break
        if ($(target).hasClass('reveal_visible')) return;

        if (target) {
            // push target into the queue (even if it's going to shift right back out)
            revealQueue.push(target);
        } else {
            // if reveal() is called by setTimeout
            revealPending = false;
        }

        if (!revealPending) {
            // reveal the first target in the queue
            $(revealQueue.shift()).addClass('reveal_visible');
            revealPending = true;
        }

        // if elements remain in the queue, reveal them after wait
        if (revealQueue.length) revealTimer = setTimeout(reveal,revealWait);
    }

    $(document).ready(function() {
        if ($('.reveal').length) {
            $('.reveal').waypoint({
                handler: function(direction) {
                    if (direction == 'down') reveal(this.element);
                },
                offset: '100%'
            });
        }
    });


    /* ============================== */
    /*  DOCUMENT READY
    /* ============================== */

    $(document).ready(function() {
        // setup helper for rh.com mobile nav collapse elements (rh_nav.js function isn't firing)
        $('#mobile-utility-nav .tray').addClass('collapse');



        /* ========================================================================================== */
        /*  SMOOTH SCROLLING
        *
        *  Automatically scrolls to anchors on a page when the "scroll" class is
        *  applied to hyperlinks.
        *
        *  scrollTopMargin provides some padding on the top so the window doesn't quite scroll
        *  ALL the way
        *
        /* ========================================================================================== */

        $('.scroll').on('click touchend', function(e) {
            var scrollAnchor = $(this.hash);
            var scrollTop = $(this).data('scroll-top') || 80;

            e.preventDefault();

            if ($(this).data('scroll-top') === 0) {
                $('html, body').animate({
                    scrollTop: scrollAnchor.offset().top
                }, 1000);
            } else {
                $('html, body').animate({
                    scrollTop: scrollAnchor.offset().top - scrollTop
                }, 1000);
            }
        });


        /* ========================================================================================== */
        /*  (DATA) BACKGROUND IMAGE
        *
        *  Finds each "background-image" data attribute, stores the
        /*  value, and applies it as inline CSS on that element.
        /* ========================================================================================== */

        // Legacy (DO NOT USE - 07/31/20)
        var $_cacheBGElems = $('[data-mobile-background-image], [data-tablet-background-image], [data-background-image]');

        function setBackgroundImages(){
            $_cacheBGElems.each(function(){
                if (window.matchMedia('(max-width: 767px)').matches && $(this).data('mobile-background-image') !== undefined) {
                    $(this).css('background-image', 'url(' + $(this).data('mobile-background-image') + ')');
                } else if (window.matchMedia('(max-width: 991px)').matches && $(this).data('tablet-background-image') !== undefined) {
                    $(this).css('background-image', 'url(' + $(this).data('tablet-background-image') + ')');
                } else {
                    $(this).css('background-image', 'url(' + $(this).data('background-image') + ')');
                }
            });
        }

        setBackgroundImages();

        $(window).on('resize', throttle(setBackgroundImages, 500));


        // Current
        var $cacheBGElems = $('[data-bg-image], [data-bg-image-sm], [data-bg-image-md], [data-bg-image-lg], [data-bg-image-xl]');

        function setBgImages(){
            $cacheBGElems.each(function(){
                if (window.matchMedia('(min-width: 1200px)').matches && $(this).data('bg-image-xl') !== undefined) {
                    $(this).css('background-image', 'url(' + $(this).data('bg-image-xl') + ')');
                } else if (window.matchMedia('(min-width: 992px)').matches && $(this).data('bg-image-lg') !== undefined) {
                    $(this).css('background-image', 'url(' + $(this).data('bg-image-lg') + ')');
                } else if (window.matchMedia('(min-width: 768px)').matches && $(this).data('bg-image-md') !== undefined) {
                    $(this).css('background-image', 'url(' + $(this).data('bg-image-md') + ')');
                } else if (window.matchMedia('(min-width: 576px)').matches && $(this).data('bg-image-sm') !== undefined) {
                    $(this).css('background-image', 'url(' + $(this).data('bg-image-sm') + ')');
                } else {
                    $(this).css('background-image', 'url(' + $(this).data('bg-image') + ')');
                }
            });
        }

        setBgImages();

        $(window).on('resize', throttle(setBgImages, 500));


        /* ========================================================================================== */
        /*  IMG TO SVG
        *
        *  Convert <img class="svg" src="image.svg" /> to inline svg
        /* ========================================================================================== */

        $('img.svg').each(function() {
            var $img = $(this),
                imgID = $img.attr('id'),
                imgClass = $img.attr('class'),
                imgURL = $img.attr('src'),
                imgWidth = $img.attr('width'),
                imgHeight = $img.attr('height');

            $.get(imgURL, function(data) {
                // Gets the SVG tag, ignores the rest
                var $svg = $(data).find('svg');

                // Adds image's ID to the SVG
                if (typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }

                // Adds image's classes to the SVG
                if (typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass);
                }

                // Adds image's width to the SVG
                if (typeof imgWidth !== 'undefined') {
                    $svg = $svg.attr('style', 'width: ' + imgWidth + 'px;');
                }

                // Adds image's height to the SVG
                if (typeof imgHeight !== 'undefined') {
                    $svg = $svg.attr('style', 'height: ' + imgHeight + 'px;');
                }

                // Adds image's width and height to the SVG
                if (typeof imgWidth && imgHeight !== 'undefined') {
                    $svg = $svg.attr('style', 'width: ' + imgWidth + 'px; height: ' + imgHeight + 'px;');
                }

                // Removes any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Checks if the viewport is set, or else it gets automatically set
                if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                    $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
                }

                // Replaces image with SVG
                $img.replaceWith($svg);
            }, 'xml');
        });


        /* ========================================================================================== */
        /*  AUTOPLAY YOUTUBE - Loads the video from the "data-src" attribute when the modal is
        /*  shown, and removes the video when the modal is closed.
        /* ========================================================================================== */

        $('.video-modal').each(function() {
            $(this).on('show.bs.modal', function() {
                var video = $(this).find('iframe.embed-responsive-item'),
                    src = video.data('src');

                video.attr('src', src);
            });

            $(this).on('hide.bs.modal', function() {
                var video = $(this).find('iframe.embed-responsive-item');

                video.attr('src', '');
            });
        });


        /* ========================================================================================== */
        /*  STICKY NAV - Applies the "sticky" class to sticky nav, on scroll.
        /* ========================================================================================== */

        var $stickyNav = $('#sticky-nav');

        if ($stickyNav.length) {
            var stickyNavHeight = $stickyNav.outerHeight(),
                navTop = $stickyNav.offset().top,
                $body = $('body'),
                $mainNav = $('#main-menu, #main-nav-wrap'),
                mainNavHeight = $mainNav.outerHeight() || null,
                scrollWait = 20,
                resizeWait = 125;

            $(window).on('scroll', throttle(function() {
                var winTop = $(window).scrollTop();

                // make .sticky-nav sticky when it hits the top of the window
                if (winTop > navTop && $stickyNav.is(":visible")) {
                    $body.css('margin-top', stickyNavHeight);
                    $stickyNav.addClass('sticky');
                } else {
                    $body.css('margin-top', '');
                    $stickyNav.removeClass('sticky');
                }

                // push the main nav out of the way if it is currently sticky
                if ($mainNav.length && ($mainNav.hasClass("pfe-sticky") || $mainNav.hasClass("scroll-to-fixed-fixed"))){
                    if(winTop > (navTop - mainNavHeight)){
                        var navY = winTop - navTop,
                        diff = mainNavHeight - Math.abs(navY);

                        // make sure the math doesn't push the nav back onto the page
                        $mainNav.css("top", (navY <= 0 ? -diff : -$mainNav.outerHeight()));
                    } else {
                        $mainNav.css("top", 0);
                    }
                }
            },scrollWait));

            // reset vars
            $(window).on('resize', throttle(function(){
                navTop = $stickyNav.offset().top;
                mainNavHeight = $mainNav.outerHeight() || null;
                stickyNavHeight = $stickyNav.outerHeight();

                $body.toggleClass('scrolled', $body.scrollTop() > mainNavHeight);
            },resizeWait));

            // update vars on load to avoid race conditions
            $(window).on('load', function(){
                navTop = $stickyNav.offset().top;
                mainNavHeight = $mainNav.outerHeight() || null;
            });
        }


        /* ========================================================================================== */
        /*  SLICK CAROUSEL - Initializes multiple carousels with their own unique IDs.
        /* ========================================================================================== */

        if($('.resource-carousel').length){
            $('.resource-carousel').each(function() {
                var carousel = $(this).attr('id'),
                    $section = $('#' + carousel).parents('section'),
                    $nav_container = $section.find('.carousel-nav');

                $('#' + carousel).slick({
                    appendArrows: $nav_container,
                    infinite: false,
                    slidesToShow: 3,
                    nextArrow:'<button class="next" title="Next"><i class="fa fa-angle-right font-size-36"></i></button>',
                    prevArrow:'<button class="prev mr-1" title="Previous"><i class="fa fa-angle-left font-size-36"></i></button>',
                    responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                        }
                    }]
                });
            });

            var slickResizeWait = 600; // need to allow > 500ms for .container max-width transition

            $(window).on('resize', throttle(function() {
                // reset slick position on resize
                $('.resource-carousel').each(function() {
                    var carousel = $(this).attr('id');
                    $('#' + carousel).slick('setPosition');
                });
            },slickResizeWait));
        }


        /* ========================================================================================== */
        /*  Poster / Video swap
        /* ========================================================================================== */

        if($('.video-poster-replace').length){
            $('.video-poster-replace .poster').on("click", function(e){
                var REPLACED = "rh.dms.video-poster-replaced";
                var $video = $(this).siblings('.video-embed').find(".embed-responsive-item");
                var $source = $video.is("video") ? $video.find("source") : null;
                var $wrapper = $(this).parent(".video-poster-replace");

                e.preventDefault();

                if(!$source){
                    // youtube -- will autoplay if param is included in src url
                    $video.attr("src", $video.data("src"));
                } else {
                    // html5 video -- will autoplay if param is included on the video tag, no need to call play()
                    // can have multiple <source> tags
                    $source.attr("src", $source.data("src"));
                    $video[0].load();
                }

                $wrapper.addClass("video-poster-replaced");


                // trigger relaced event
                $wrapper.trigger( {type:REPLACED} );
            });
        }



        /* ========================================================================================== */
        /*  Accordion panel toggle "showing" class
        /* ========================================================================================== */

        if($('.accordion-panel').length){
            $('.accordion-panel').on('show.bs.collapse', function (e) {
            $(e.currentTarget).addClass("showing");
            });

            $('.accordion-panel').on('shown.bs.collapse', function (e) {
            $(e.currentTarget).removeClass("showing");
            })
        }
        /* ========================================================================================== */
        /*  Card: Vertical Sliding info on hover
        *
        *  On hover the image is coverd with a card with more information.
        /*  Uses a simple toggle.
        /* ========================================================================================== */
        $(".card-vslide").on("click", function(){
            $(this).toggleClass("active");
        });


        /* ========================================================================================== */
        /*  Card: Background halves on hover
        *
        *  On hover background is shrunk to fill only the top half of the card. Revealing information.
        /*  Adds and removes classes as necessary.
        /* ========================================================================================== */



        var $cardHalfSlide = $(".card-half-slide");
        $cardHalfSlide.each(function(i) {
            int = (3*i)+1;
            $cardHalfSlide.eq(int).addClass( "default" ).addClass( "active" );
        });

        $cardHalfSlide.hover(
            function(){
                // de-activate cards in same row
                $(this).parents(".row").find(".card-half-slide").removeClass("active");
                $(this).addClass("active");
            },
            function(){
                // de-activate cards in same row
                $(this).parents(".row").find(".card-half-slide").removeClass("active");
                // activate default card
                $(this).parents(".row").find(".card-half-slide.default").addClass("active");
            }
        );

    });
})(jQuery);