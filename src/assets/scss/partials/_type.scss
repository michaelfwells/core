/* ========================================================================================== */
/*  Override bootstrap body size default (uses $font-size-base) to match redhat.com
/* ========================================================================================== */

html {
    font-size: 62.5%;
}

body {
    font-size: $font-size-px-base;
    font-size: var(--pfe-theme--font-size,var(--pfe-theme--base--text, $font-size-px-base)); //16px
    
    @media (min-width: breakpoint-min(md)) {
        font-size: var(--pfe-theme--font-size,var(--pfe-theme--base--text, $font-size-px-base * 1.125)); //18px
    }
}


/* ========================================================================================== */
/*  Removing text-rending -smoothing, rely on primary css for this
/* ========================================================================================== */

/*
* {
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
*/


/* ========================================================================================== */
/*  Font sizes are determined by the following logic:
/*  http://www.carbondesignsystem.com/style/typography/overview#type-scale
/*
/*  This creates font size classes like:
/*  .font-size-12
/*  .font-size-md-12
/*  .font-size-lg-12
/*  .font-size-xl-12
/*
/*  NOTE: We can add more sizes, but we want to be reasonable.
/* ========================================================================================== */

$font-sizes: (12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48);

@each $breakpoint in map-keys($grid-breakpoints) {
    @include media-breakpoint-up($breakpoint) {
        $infix: breakpoint-infix($breakpoint, $grid-breakpoints);

        @each $size in $font-sizes {
            .font-size#{$infix}-#{$size} {
                font-size: $size + px !important;
            }
        }
    }
}



/* ========================================================================================== */
/*  Set default type sizes for .dms-band / .at content
 *
 *  $type_ns var sets the namespace to one of the above classes and applies to all
 *  typography styles
/* ========================================================================================== */

@at-root #{$type_ns} {

    p,
    li {
        margin-top: $paragraph-margin-top;
        font-size: $font-size-px-base;
        font-size: var(--pfe-theme--font-size, $font-size-px-base); // 16px
        
        @media (min-width: breakpoint-min(md)) {
            font-size: $font-size-px-base * 1.125;
            font-size: var(--pfe-theme--font-size, $font-size-px-base * 1.125); // 18px
        }
    }
    
    
    h1, h2, h3, h4, h5, h6 {
      margin-top: $headings-margin-top;
      margin-bottom: $headings-margin-bottom;
      font-family: $headings-font-family;
    }

    h1, .xl {
        font-size: $font-size-px-base * 1.8125;
        font-size: var(--pfe-theme--font-size--xl, $font-size-px-base * 1.8125);  // 29px
        font-weight: var(--pfe-theme--font-weight--xl, $font-weight-light);
        line-height: var(--pfe-theme--line-height--xl, 1.2);
        
        @media (min-width: breakpoint-min(md)) {
            font-size: 29px;
            font-size: var(--pfe-theme--font-size--xl, $font-size-px-base * 2.5);  // 40px
        }
    }

    .xxl{
        font-size: $font-size-px-base * 2.1875;
        font-size: var(--pfe-theme--font-size--xxl, $font-size-px-base * 2.1875);  // 35px
        font-weight: var(--pfe-theme--font-weight--xxl, $font-weight-light);
        line-height: var(--pfe-theme--line-height--xxl, 1.2);
        
        @media (min-width: breakpoint-min(md)) {
            font-size: $font-size-px-base * 3;
            font-size: var(--pfe-theme--font-size--xxl, $font-size-px-base * 3);  // 48px
        }
    }
    
    // TODO: UPDATE TO --alpha
    h2 {
        font-size: $font-size-px-base * 1.5;
        font-size: var(--pfe-theme--font-size--beta, $font-size-px-base * 1.5);  // 24px
        font-weight: var(--pfe-theme--font-weight--beta, $font-weight-normal);
        line-height: var(--pfe-theme--line-height--beta, 1.3);
        
        @media (min-width: breakpoint-min(md)) {
            font-size: $font-size-px-base * 1.75;
            font-size: var(--pfe-theme--font-size--beta, $font-size-px-base * 1.75);  // 28px
        }
    }
    
    // TODO: UPDATE TO --beta
    h3 {
        font-size: $font-size-px-base * 1.25;
        font-size: var(--pfe-theme--font-size--gamma, $font-size-px-base * 1.25);  // 20px
        font-weight: var(--pfe-theme--font-weight--gamma, $font-weight-normal);
        line-height: var(--pfe-theme--line-height--gamma, 1.3);
        
        @media (min-width: breakpoint-min(md)) {
            font-size: $font-size-px-base * 1.5;
            font-size: var(--pfe-theme--font-size--gamma, $font-size-px-base * 1.5);  // 24px
        }
    }
    
    // typically h3's
    .section-label {
        font-size: $font-size-px-base * 1.125;
        font-size: var(--pfe-theme--font-size--epsilon, $font-size-px-base * 1.125);  // 18px
        font-weight: var(--pfe-theme--font-weight--delta, $font-weight-normal);
        line-height: var(--pfe-theme--line-height--delta, 1.5);
        text-transform: uppercase;
        
        @media (min-width: breakpoint-min(md)) {
            font-size: $font-size-px-base * 1.25;
            font-size: var(--pfe-theme--font-size--delta, $font-size-px-base * 1.25);  // 20px
        }
    }

    .card-label {
        font-size: $font-size-px-base;
        font-size: var(--pfe-theme--font-size--default-sm, $font-size-px-base);  // 16px
        font-weight: var(--pfe-theme--font-weight, $font-weight-normal);
        line-height: var(--pfe-theme--line-height, 1.5);
        text-transform: uppercase;
        
        // no adjustment for mobile
    }
    
    // TODO: UPDATE TO --gamma
    h4 {
        font-size: $font-size-px-base * 1.25;
        font-size: var(--pfe-theme--font-size--delta, $font-size-px-base * 1.25);  // 20px
        font-weight: var(--pfe-theme--font-weight--delta, $font-weight-normal);
        line-height: var(--pfe-theme--line-height--delta, 1.5);
        
        @media (min-width: breakpoint-min(md)) {
            font-size: $font-size-px-base * 1.25;
            font-size: var(--pfe-theme--font-size--delta, $font-size-px-base * 1.25);  // no change from mobile
        }
    }

    // TODO: UPDATE TO --delta
    h5 {
        text-transform: uppercase;
        font-size: $font-size-px-base * 1.125;
        font-size: var(--pfe-theme--font-size--epsilon, $font-size-px-base * 1.125);  // todo 18px
        font-weight: var(--pfe-theme--font-weight--epsilon, $font-weight-normal);
        line-height: var(--pfe-theme--line-height--epsilon, 1.5);
        
        @media (min-width: breakpoint-min(md)) {
            font-size: $font-size-px-base * 1.125;
            font-size: var(--pfe-theme--font-size--epsilon, $font-size-px-base * 1.125);  // no change from mobile
        }
    }

    h6 {
        font-weight: $font-weight-bold;
    }

    p.lead {
        font-size: $font-size-px-base * 1.125;
        font-size: var(--pfe-theme--font-size--default-lg, $font-size-px-base * 1.125); // 18px
        font-weight: var(--pfe-theme--font-weight, $font-weight-normal);      


        @media (min-width: breakpoint-min(md)) {
            font-size: $font-size-px-base * 1.25;
            font-size: var(--pfe-theme--font-size--default-lg, $font-size-px-base * 1.25);  // 20px
        }
    }

    small,
    .small{
        font-size: $small-font-size;
        font-size: var(--pfe-theme--font-size--default-xs, $small-font-size); // 14px
    }
    
    hr {
        width: 100%;
        margin-top: $hr-margin-y;
        border-bottom: $hr-margin-y;
    
        &.sm {
            width: 42px;
            margin-right: auto;
            margin-left: 0;
        }
    
        &.x2 {
            border-top-width: 2px;
        }
    
        &.x3 {
            border-top-width: 3px;
        }
    }
}

@each $color, $value in $theme-colors {
    .rule-#{$color}:after {
        display: block;
        width: 30px;
        height: 2px;
        margin-top: 10px;
        content: "";
        background-color: $value;
    }
}

.text-normal {
    text-transform: none !important;
}

.line-height-0 {
    line-height: 0px;
}

.font-family {
    &-text {
        font-family: $font-family-sans-serif !important;
    }
    
    &-display {
        font-family: $headings-font-family !important;
    }

    &-monospace {
        font-family: $font-family-monospace !important;
    }
}

a:hover {
    .btn-link {
        .fa-angle-right:before {
            left: 5px;
            transition: left 0.5s ease;
        }
    }
}


