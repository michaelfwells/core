/*!
 * Physics2DPlugin 3.0.4
 * https://greensock.com
 *
 * @license Copyright 2019, GreenSock. All rights reserved.
 * Subject to the terms at https://greensock.com/standard-license or for Club GreenSock members, the agreement issued with that membership.
 * @author: Jack Doyle, jack@greensock.com
 */

!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?t(exports):"function"==typeof define&&define.amd?define(["exports"],t):t((e=e||self).window=e.window||{})}(this,function(e){"use strict";function h(){return t||"undefined"!=typeof window&&(t=window.gsap)&&t.registerPlugin&&t}function i(e){return Math.round(1e4*e)/1e4}function j(e){t=e||h(),l||(o=t.utils.getUnit,l=1)}function k(e,t,i,s,n){var a=e._gsap,r=a.get(e,t);this.p=t,this.set=a.set(e,t),this.s=this.val=parseFloat(r),this.u=o(r)||0,this.vel=i||0,this.v=this.vel/n,s||0===s?(this.acc=s,this.a=this.acc/(n*n)):this.acc=this.a=0}var t,l,o,v=Math.PI/180,s={version:"3.0.4",name:"physics2D",register:j,init:function init(e,t,i){l||j();var s=this,n=+t.angle||0,a=+t.velocity||0,r=+t.acceleration||0,o=t.xProp||"x",p=t.yProp||"y",c=t.accelerationAngle||0===t.accelerationAngle?+t.accelerationAngle:n;s.target=e,s.tween=i,s.step=0,s.sps=30,t.gravity&&(r=+t.gravity,c=90),n*=v,c*=v,s.fr=1-(+t.friction||0),s._props.push(o,p),s.xp=new k(e,o,Math.cos(n)*a,Math.cos(c)*r,s.sps),s.yp=new k(e,p,Math.sin(n)*a,Math.sin(c)*r,s.sps),s.skipX=s.skipY=0},render:function render(e,t){var s,n,a,r,o,p,c=t.xp,l=t.yp,v=t.tween,f=t.target,h=t.step,u=t.sps,d=t.fr,g=t.skipX,y=t.skipY,w=v._from?v._dur-v._time:v._time;if(1===t.fr)a=w*w*.5,s=c.s+(c.vel*w+c.acc*a),n=l.s+(l.vel*w+l.acc*a);else{if(r=p=(0|(w*=u))-h,o=w%1,0<=p)for(;p--;)c.v+=c.a,l.v+=l.a,c.v*=d,l.v*=d,c.val+=c.v,l.val+=l.v;else for(p=-p;p--;)c.val-=c.v,l.val-=l.v,c.v/=d,l.v/=d,c.v-=c.a,l.v-=l.a;s=c.val+c.v*o,n=l.val+l.v*o,t.step+=r}g||c.set(f,c.p,i(s)+c.u),y||l.set(f,l.p,i(n)+l.u)},kill:function kill(e){this.xp.p===e&&(this.skipX=1),this.yp.p===e&&(this.skipY=1)}};h()&&t.registerPlugin(s),e.Physics2DPlugin=s,e.default=s,Object.defineProperty(e,"__esModule",{value:!0})});
//# sourceMappingURL=Physics2DPlugin.min.js.map
